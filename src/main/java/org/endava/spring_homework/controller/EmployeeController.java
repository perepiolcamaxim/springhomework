package org.endava.spring_homework.controller;

import lombok.RequiredArgsConstructor;
import org.endava.spring_homework.DTO.EmployeesDTO;
import org.endava.spring_homework.assembler.EmployeeDTOAssembler;
import org.endava.spring_homework.entity.Employees;
import org.endava.spring_homework.mapper.EmployeesMapper;
import org.endava.spring_homework.service.EmployeeService;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/employees")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;
    private final EmployeesMapper mapper;
    private final EmployeeDTOAssembler assembler;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public CollectionModel<EntityModel<EmployeesDTO>> getAllEmployees(@RequestParam(required = false, defaultValue = "false") boolean expand) {
        return assembler.toCollectionModel(employeeService
                .getAll(expand)
                .stream()
                .map(mapper::employeeToDTO)
                .collect(Collectors.toList()));
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{id}")
    public EntityModel<EmployeesDTO> getEmployeeById(@PathVariable long id){
        return assembler.toModel(mapper.employeeToDTO(employeeService.findEmployeeById(id)));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public EntityModel<EmployeesDTO> createEmployee(@RequestBody @Valid EmployeesDTO employeeDTO) {
        return assembler.toModel(
                mapper.employeeToDTO(employeeService.saveEmployee(mapper.dtoToEmployee(employeeDTO))));
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("{id}")
    public void updateEmployee(@PathVariable long id, @RequestBody @Valid EmployeesDTO employeeDTO){
        Employees employees = employeeService.findEmployeeById(id);
        mapper.updatePatchEmployee(employeeDTO, employees);
        employeeService.saveEmployee(employees);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("{id}")
    public void deleteEmployee(@PathVariable Long id){
        employeeService.deleteEmployee(id);
    }
}
