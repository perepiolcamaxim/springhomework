package org.endava.spring_homework.controller;

import lombok.RequiredArgsConstructor;
import org.endava.spring_homework.DTO.DepartmentsDTO;
import org.endava.spring_homework.assembler.DepartmentDTOAssembler;
import org.endava.spring_homework.entity.Departments;
import org.endava.spring_homework.mapper.DepartmentsMapper;
import org.endava.spring_homework.service.DepartmentService;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/departments")
public class DepartmentController {

    private final DepartmentService departmentService;
    private final DepartmentsMapper mapper;
    private final DepartmentDTOAssembler assembler;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public CollectionModel<EntityModel<DepartmentsDTO>>
                    getAllDepartments(@RequestParam(required = false, defaultValue = "false") boolean expand){

        return assembler.toCollectionModel(departmentService
                .getAllDepartments(expand)
                .stream()
                .map(mapper::departmentToDTO)
                .collect(Collectors.toList()));
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{id}")
    public EntityModel<DepartmentsDTO> findDepartmentById(@PathVariable Long id){
        return assembler.toModel(mapper.departmentToDTO(departmentService.findDepartmentById(id)));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public EntityModel<DepartmentsDTO> saveDepartment(@RequestBody @Valid DepartmentsDTO departmentDTO){
        return assembler.toModel(
                mapper.departmentToDTO(departmentService.addDepartment(mapper.dtoToDepartment(departmentDTO))));
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("{id}")
    public void updateDepartment(@PathVariable Long id, @RequestBody @Valid DepartmentsDTO departmentDTO){
        Departments departments = departmentService.findDepartmentById(id);
        mapper.updatePatchDepartment(departmentDTO, departments);
        departmentService.addDepartment(departments);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("{id}")
    public void deleteDepartment(@PathVariable Long id){
        departmentService.deleteDepartmentById(id);
    }
}
