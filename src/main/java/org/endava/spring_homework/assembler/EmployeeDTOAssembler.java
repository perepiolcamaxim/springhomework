package org.endava.spring_homework.assembler;

import org.endava.spring_homework.DTO.EmployeesDTO;
import org.endava.spring_homework.controller.EmployeeController;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class EmployeeDTOAssembler implements RepresentationModelAssembler<EmployeesDTO, EntityModel<EmployeesDTO>> {
    @Override
    public EntityModel<EmployeesDTO> toModel(EmployeesDTO entity) {
        return EntityModel.of(entity,
                linkTo(methodOn(EmployeeController.class).getEmployeeById(entity.getId())).withSelfRel(),
                linkTo(methodOn(EmployeeController.class).getAllEmployees(true)).withRel("employees"));
    }
}
