package org.endava.spring_homework.assembler;

import org.endava.spring_homework.DTO.DepartmentsDTO;
import org.endava.spring_homework.controller.DepartmentController;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class DepartmentDTOAssembler implements RepresentationModelAssembler<DepartmentsDTO, EntityModel<DepartmentsDTO>> {
    @Override
    public EntityModel<DepartmentsDTO> toModel(DepartmentsDTO entity) {
        return EntityModel.of(entity,
                linkTo(methodOn(DepartmentController.class).findDepartmentById(entity.getId())).withSelfRel(),
                linkTo(methodOn(DepartmentController.class).getAllDepartments(true)).withRel("departments"));
    }
}
