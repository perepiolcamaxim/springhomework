package org.endava.spring_homework.mapper;

import org.endava.spring_homework.DTO.EmployeesDTO;
import org.endava.spring_homework.entity.Employees;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface EmployeesMapper {

    @Mapping(source = "department.id", target = "departmentId")
    @Mapping(source = "manager.id", target = "managerId")
    EmployeesDTO employeeToDTO(Employees employees);

    @InheritInverseConfiguration
    @Mapping(target = "id", ignore = true)
    Employees dtoToEmployee(EmployeesDTO employeesDTO);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(source = "departmentId", target = "department.id")
    @Mapping(source = "managerId", target = "manager.id")
    @Mapping(target = "id", ignore = true)
    void updatePatchEmployee(EmployeesDTO employeeDTO, @MappingTarget Employees employee);
}
