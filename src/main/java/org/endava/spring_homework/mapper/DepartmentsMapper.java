package org.endava.spring_homework.mapper;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.endava.spring_homework.DTO.DepartmentsDTO;
import org.endava.spring_homework.entity.Departments;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface DepartmentsMapper {

    @Mapping(source = "manager.id", target = "managerId")
    DepartmentsDTO departmentToDTO(Departments employees);

    @InheritInverseConfiguration
    @Mapping(target = "id", ignore = true)
    Departments dtoToDepartment(DepartmentsDTO employeesDTO);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(source = "managerId", target = "manager.id")
    @Mapping(target = "id", ignore = true)
    void updatePatchDepartment(DepartmentsDTO departmentDTO, @MappingTarget Departments department);
}
