package org.endava.spring_homework.DTO;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.endava.spring_homework.entity.Departments;
import org.endava.spring_homework.entity.Employees;
import org.endava.spring_homework.entity.Jobs;
import org.endava.spring_homework.validator.PhoneNumber;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.sql.Date;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeesDTO {
    private Long id;
    @NotBlank(message = "First name must not be null, blank or empty")
    private String firstName;
    @NotBlank(message = "Last name must not be null, blank or empty")
    private String lastName;
    @Email(message = "Email must be valid")
    private String email;
    @PhoneNumber
    private String phoneNumber;
    private Date hireDate;
    @Min(value = 1, message = "Salary should be min 1")
    private Integer salary;
    private BigDecimal commissionPCT;
    private Jobs job;

    private Long managerId;

    private Integer departmentId;
}
