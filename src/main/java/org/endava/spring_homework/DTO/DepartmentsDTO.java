package org.endava.spring_homework.DTO;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.endava.spring_homework.entity.Employees;
import org.endava.spring_homework.entity.Locations;
import javax.validation.constraints.NotBlank;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@AllArgsConstructor
@Data
@Builder
public class DepartmentsDTO {
    private Long id;
    @NotBlank(message = "Department name must not be null, blank or empty")
    private String departmentName;

    private Integer managerId;
    private Locations location;
}
