package org.endava.spring_homework.service.impl;

import lombok.RequiredArgsConstructor;
import org.endava.spring_homework.entity.Employees;
import org.endava.spring_homework.repository.EmployeeRepository;
import org.endava.spring_homework.service.EmployeeService;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl  implements EmployeeService {
    private final EmployeeRepository employeeRepository;

    @Override
    public List<Employees> getAll(boolean expand) {
        return expand ? employeeRepository.findAll() : employeeRepository.getNameAndDepartmentAll();
    }

    @Override
    public Employees findEmployeeById(long id) {
        return employeeRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Employee with id not found"));
    }

    @Override
    public Employees saveEmployee(Employees employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
    }
}