package org.endava.spring_homework.service.impl;

import lombok.RequiredArgsConstructor;
import org.endava.spring_homework.entity.Departments;
import org.endava.spring_homework.repository.DepartmentRepository;
import org.endava.spring_homework.service.DepartmentService;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentRepository;

    @Override
    public List<Departments> getAllDepartments(boolean expand) {
        return expand ? departmentRepository.findAll() : departmentRepository.findAllDepartmentsName();
    }

    @Override
    public Departments findDepartmentById(Long id) {
        return departmentRepository.findById(id).orElseThrow(()-> new NoSuchElementException("Department with id not found"));
    }

    @Override
    public Departments addDepartment(Departments department) {
        return departmentRepository.save(department);
    }

    @Override
    public void deleteDepartmentById(Long id) {
        departmentRepository.deleteById(id);
    }
}
