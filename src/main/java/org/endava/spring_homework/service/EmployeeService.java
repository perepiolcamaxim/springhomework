package org.endava.spring_homework.service;

import org.endava.spring_homework.entity.Employees;
import java.util.List;

public interface EmployeeService {
    List<Employees> getAll(boolean expand);

    Employees saveEmployee(Employees employee);

    void deleteEmployee(Long id);

    Employees findEmployeeById(long id);
}