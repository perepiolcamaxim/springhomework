package org.endava.spring_homework.service;

import org.endava.spring_homework.entity.Departments;
import java.util.List;

public interface DepartmentService {
    List<Departments> getAllDepartments(boolean expand);

    Departments findDepartmentById(Long id);

    Departments addDepartment(Departments department);

    void deleteDepartmentById(Long id);
}
