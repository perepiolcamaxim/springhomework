package org.endava.spring_homework.repository;

import org.endava.spring_homework.entity.Departments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DepartmentRepository extends JpaRepository<Departments, Long> {
    @Query("select new Departments (d.departmentName) from Departments d")
    List<Departments> findAllDepartmentsName();
}
