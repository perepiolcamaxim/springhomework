package org.endava.spring_homework.repository;

import org.endava.spring_homework.entity.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employees, Long> {

    @Query("select new Employees (e.firstName, e.lastName, e.department) from Employees e")
     List<Employees> getNameAndDepartmentAll();
}