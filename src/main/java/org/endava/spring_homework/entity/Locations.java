package org.endava.spring_homework.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
public class Locations {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "location_seq_generator")
    @SequenceGenerator(name = "location_seq_generator", sequenceName = "locations_seq", initialValue = 3300, allocationSize = 100)
    @Column(name = "location_id")
    private Integer locationId;
    private String streetAddress;
    private String postalCode;
    private String city;
    private String stateProvince;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "country_id")
    private Countries country;
}
