package org.endava.spring_homework.entity;

import lombok.Data;
import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
public class Countries {

    @Id
    @Column(name = "country_id", length = 2, columnDefinition = "char")
    private String id;
    private String countryName;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "region_id")
    private Regions region;
}
