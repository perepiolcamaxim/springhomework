package org.endava.spring_homework.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Employees {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_seq_generator")
    @SequenceGenerator(name = "employee_seq_generator", sequenceName = "employees_seq", initialValue = 207, allocationSize = 1)
    @Column(name = "employee_id")
    private Long id;

    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private Date hireDate;
    private Integer salary;

    @Column(name = "commission_pct", length = 2, precision = 2)
    private BigDecimal commissionPCT;

    @ManyToOne
    @JoinColumn(name = "job_id")
    private Jobs job;

    @ManyToOne
    @JoinColumn(name = "manager_id")
    private Employees manager;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Departments department;

    public Employees(String firstName, String lastName, Departments department) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
    }
}