package org.endava.spring_homework.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Departments {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "departments_seq_generator")
    @SequenceGenerator(name = "departments_seq_generator", sequenceName = "departments_seq", initialValue = 280, allocationSize = 10)
    @Column(name = "department_id")
    private Long id;

    private String departmentName;

    @OneToOne
    @JoinColumn(name = "manager_id")
    private Employees manager;

    @ManyToOne
    @JoinColumn(name = "location_id")
    private Locations location;

    public Departments(String departmentName) {
        this.departmentName = departmentName;
    }
}
