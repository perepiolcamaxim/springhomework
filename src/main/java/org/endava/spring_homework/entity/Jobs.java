package org.endava.spring_homework.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@NoArgsConstructor
public class Jobs {

    @Id
    @Column(name = "job_id")
    private String id;

    private String jobTitle;
    private Integer minSalary;
    private Integer maxSalary;

    public Jobs(String id) {
        this.id = id;
    }
}
