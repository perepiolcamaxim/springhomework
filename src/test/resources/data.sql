INSERT INTO REGIONS(REGION_ID, REGION_NAME) VALUES (1, 'Europe');

INSERT INTO COUNTRIES(COUNTRY_ID, COUNTRY_NAME, REGION_ID) VALUES ('MD', 'Moldova', 1);

INSERT INTO LOCATIONS(LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, STATE_PROVINCE, COUNTRY_ID) VALUES
(1, 'Studentilor', '2021', 'Chisinau', 'null', 'MD');

INSERT INTO DEPARTMENTS(DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID, LOCATION_ID) VALUES
(1, 'Marketing', NULL, 1);

INSERT INTO DEPARTMENTS(DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID, LOCATION_ID) VALUES
(2, 'IT', NULL, 1);

INSERT INTO JOBS(JOB_ID, JOB_TITLE, MIN_SALARY, MAX_SALARY)
VALUES ('AD_PRES', 'President', 20080, 40000);

INSERT INTO
    EMPLOYEES(EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY, COMMISSION_PCT, MANAGER_ID, DEPARTMENT_ID)
VALUES (1, 'Perepiolca', 'Maxim', 'perepiolcamaxim@gmail.com', '079751321', date '2009-12-3', 'AD_PRES', 5000, NULL, NULL, 1);

INSERT INTO
    EMPLOYEES(EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY, COMMISSION_PCT, MANAGER_ID, DEPARTMENT_ID)
VALUES (2, 'Moraru', 'Ion', 'moraru@gmail.com', '079751322', date '2009-12-3', 'AD_PRES', 5000, NULL, NULL, 1);