package org.endava.spring_homework.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.endava.spring_homework.DTO.DepartmentsDTO;
import org.endava.spring_homework.entity.Departments;
import org.endava.spring_homework.entity.Locations;
import org.endava.spring_homework.mapper.DepartmentsMapper;
import org.endava.spring_homework.service.DepartmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import java.util.Collections;
import java.util.List;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(DepartmentController.class)
public class DepartmentControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private DepartmentsMapper departmentsMapper;

    @MockBean
    private DepartmentService departmentService;

    private DepartmentsDTO departmentDTO;

    private String departmentName = "IT";
    private Long departmentId = 1L;
    public Departments department;

    @BeforeEach
    public void setup(){
        createDepartment();
        createDepartmentDTO();
    }

    @Test
    public void givenDepartments_whenGetDepartments_thenReturnJsonArray() throws Exception {
        List<Departments> list = Collections.singletonList(department);

        given(departmentsMapper.departmentToDTO(department)).willReturn(departmentDTO);
        given(departmentService.getAllDepartments(true)).willReturn(list);

        mvc.perform(get("/api/departments?expand=true")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].departmentName").exists());
    }

    @Test
    public void givenDepartments_whenGetDepartmentsExpandFalse_thenReturnJsonArray() throws Exception {
        DepartmentsDTO PARTIAL_DEPARTMENT_DTO =
                new DepartmentsDTO(0L, "Marketing", 0, null);

        department.setId(0L);
        List<Departments> list = Collections.singletonList(department);

        given(departmentService.getAllDepartments(false)).willReturn(list);
        given(departmentsMapper.departmentToDTO(department)).willReturn(PARTIAL_DEPARTMENT_DTO);

        mvc.perform(get("/api/departments?expand=false")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].departmentName").exists());
    }

    @Test
    public void givenDepartment_whenGetDepartmentById_thenReturnJson() throws Exception {

        given(departmentsMapper.departmentToDTO(department)).willReturn(departmentDTO);
        given(departmentService.findDepartmentById(department.getId())).willReturn(department);

        mvc.perform(get("/api/departments/{id}", department.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.departmentName", is("IT")));
    }

    @Test
    public void testSaveDepartment() throws Exception {

        given(departmentsMapper.dtoToDepartment(departmentDTO)).willReturn(department);
        given(departmentsMapper.departmentToDTO(department)).willReturn(departmentDTO);
        given(departmentService.addDepartment(department)).willReturn(department);

        mvc.perform( MockMvcRequestBuilders
                .post("/api/departments")
                .content(asJsonString(departmentDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    @Test
    public void testUpdateDepartment() throws Exception {

        departmentDTO.setDepartmentName("Management");
        given(departmentService.findDepartmentById(departmentDTO.getId())).willReturn(department);

        department.setDepartmentName("Management");
        mvc.perform( MockMvcRequestBuilders
                .put("/api/departments/{id}", departmentDTO.getId())
                .content(asJsonString(departmentDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(departmentsMapper).updatePatchDepartment(departmentDTO, department);
        Mockito.verify(departmentService).findDepartmentById(departmentDTO.getId());
        Mockito.verify(departmentService).addDepartment(department);
    }

    @Test
    public void deleteDepartment() throws Exception {
        doThrow(EmptyResultDataAccessException.class).when(departmentService).deleteDepartmentById(1000L);

        mvc.perform( MockMvcRequestBuilders
                .delete("/api/departments/1000")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void createDepartmentDTO() {
        departmentDTO = DepartmentsDTO
                .builder()
                .id(departmentId)
                .departmentName(departmentName)
                .location(new Locations())
                .managerId(1)
                .build();
    }

    private void createDepartment() {
        department = Departments.builder()
                .departmentName(departmentName)
                .id(departmentId)
                .build();
    }
}
