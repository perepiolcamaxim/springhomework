package org.endava.spring_homework.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.endava.spring_homework.DTO.EmployeesDTO;
import org.endava.spring_homework.EmployeeSupplier;
import org.endava.spring_homework.entity.Employees;
import org.endava.spring_homework.entity.Jobs;
import org.endava.spring_homework.mapper.EmployeesMapper;
import org.endava.spring_homework.service.EmployeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.endava.spring_homework.EmployeeSupplier.createEmployee;
import static org.endava.spring_homework.EmployeeSupplier.createEmployeeDTO;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private EmployeesMapper employeesMapper;

    @MockBean
    private EmployeeService employeeService;

    public EmployeesDTO employeeDTO;
    private Employees employee;

    @BeforeEach
    public void setup(){
        employee = createEmployee();
        employeeDTO = createEmployeeDTO();
    }

    @Test
    public void givenEmployees_whenGetEmployeesExpandTrue_thenReturnJsonArray() throws Exception {
        List<Employees> list = Collections.singletonList(employee);

        given(employeesMapper.employeeToDTO(employee)).willReturn(employeeDTO);
        given(employeeService.getAll(true)).willReturn(list);

        mvc.perform(get("/api/employees?expand=true")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].email").exists());
    }

    @Test
    public void givenEmployees_whenGetEmployeesExpandFalse_thenReturnJsonArray() throws Exception {
        EmployeesDTO PARTIAL_EMPLOYEE_DTO = new EmployeesDTO(null, "Per", "Max", null,
                null, null , null, null, null,
                null, 1);

        List<Employees> list = Collections.singletonList(employee);

        given(employeeService.getAll(false)).willReturn(list);
        given(employeesMapper.employeeToDTO(employee)).willReturn(PARTIAL_EMPLOYEE_DTO);

        mvc.perform(get("/api/employees?expand=false")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].firstName").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].email").doesNotExist());
    }

    @Test
    public void givenEmployee_whenGetEmployeeById_thenReturnJson() throws Exception {

        employee.setId(employeeDTO.getId());
        given(employeesMapper.employeeToDTO(employee)).willReturn(employeeDTO);
        given(employeeService.findEmployeeById(employee.getId())).willReturn(employee);

        mvc.perform(get("/api/employees/{id}", employeeDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(3)));
    }

    @Test
    public void testSaveEmployee() throws Exception {

        given(employeesMapper.dtoToEmployee(employeeDTO)).willReturn(employee);
        given(employeesMapper.employeeToDTO(employee)).willReturn(employeeDTO);
        given(employeeService.saveEmployee(employee)).willReturn(employee);

        mvc.perform( MockMvcRequestBuilders
                .post("/api/employees")
                .content(asJsonString(employeeDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    @Test
    public void testUpdateEmployee() throws Exception {

        mvc.perform( MockMvcRequestBuilders
                .put("/api/employees/1")
                .content(asJsonString(employeeDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteEmployee() throws Exception {
        doThrow(EmptyResultDataAccessException.class).when(employeeService).deleteEmployee(1000L);

        mvc.perform( MockMvcRequestBuilders
                .delete("/api/employees/1000")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
