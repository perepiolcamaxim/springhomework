package org.endava.spring_homework.repository;

import org.endava.spring_homework.entity.Departments;
import org.endava.spring_homework.entity.Employees;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class EmployeeRepositoryTest {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void whenGetAllDepartmentsByName_thenReturnThem(){

        List<Employees> list = employeeRepository.getNameAndDepartmentAll();

        assertEquals("Perepiolca", list.get(0).getFirstName());
        assertEquals("Maxim", list.get(0).getLastName());
        assertNull(list.get(0).getId());
        assertNull(list.get(0).getEmail());
        assertEquals("Moraru", list.get(1).getFirstName());
        assertEquals("Ion", list.get(1).getLastName());
        assertNull(list.get(1).getId());
        assertNull(list.get(1).getEmail());
    }
}
