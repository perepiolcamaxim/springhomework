package org.endava.spring_homework.repository;

import org.endava.spring_homework.entity.Departments;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class DepartmentRepositoryTest {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Test
    public void whenGetAllDepartmentsByName_thenReturnThem(){

        List<Departments> allDepartmentsName = departmentRepository.findAllDepartmentsName();

        assertEquals("Marketing", allDepartmentsName.get(0).getDepartmentName());
        assertEquals("IT", allDepartmentsName.get(1).getDepartmentName());
    }
}
