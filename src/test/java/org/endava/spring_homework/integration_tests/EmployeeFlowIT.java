package org.endava.spring_homework.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.endava.spring_homework.DTO.EmployeesDTO;
import org.endava.spring_homework.entity.Employees;
import org.endava.spring_homework.entity.Jobs;
import org.endava.spring_homework.repository.EmployeeRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.endava.spring_homework.EmployeeSupplier.createEmployeeDTO;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import java.sql.Date;
import java.time.LocalDate;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.hamcrest.Matchers.is;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EmployeeFlowIT {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private EmployeeRepository employeeRepository;

    private EmployeesDTO EMPLOYEE_DTO;

    @BeforeEach
    public void setup(){
        EMPLOYEE_DTO = createEmployeeDTO();
    }

    @Test
    @Order(1)
    public void getAllWithExpandTrue() throws Exception {
        this.mvc.perform(get("/api/employees?expand=true")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].email").exists());
    }

    @Test
    @Order(2)
    public void getAllWithExpandFalse() throws Exception {
        this.mvc.perform(get("/api/employees?expand=false")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].firstName").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].email").doesNotExist());
    }

    @Test
    public void getById() throws Exception {
        this.mvc.perform(get("/api/employees/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void getByIdShouldReturnNotFoundWhenInvalidId() throws Exception {
        this.mvc.perform(get("/api/employees/{id}", 1000)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteEmployee() throws Exception {
        this.mvc.perform(delete("/api/employees/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        assertFalse(employeeRepository.findById(2L).isPresent());
    }

    @Test
    public void deleteEmployeeShouldReturnNotFoundWhenInvalidId() throws Exception {
        this.mvc.perform(delete("/api/employees/1000")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
        assertFalse(employeeRepository.findById(1000L).isPresent());
    }

    @Test
    public void createEmployee() throws Exception
    {
        mvc.perform( MockMvcRequestBuilders
                .post("/api/employees")
                .content(asJsonString(EMPLOYEE_DTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    @Test
    public void createEmployeeWithInvalidEmail() throws Exception
    {
        EMPLOYEE_DTO.setEmail("sdcasaxasxa");
        mvc.perform( MockMvcRequestBuilders
                .post("/api/employees")
                .content(asJsonString(EMPLOYEE_DTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createEmployeeWithInvalidPhoneNumber() throws Exception
    {
        EMPLOYEE_DTO.setPhoneNumber("123456789");
        mvc.perform( MockMvcRequestBuilders
                .post("/api/employees")
                .content(asJsonString(EMPLOYEE_DTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createEmployeeWithInvalidName() throws Exception
    {
        EMPLOYEE_DTO.setFirstName("");
        mvc.perform( MockMvcRequestBuilders
                .post("/api/employees")
                .content(asJsonString(EMPLOYEE_DTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateEmployee() throws Exception
    {
        EMPLOYEE_DTO.setId(1L);
        EMPLOYEE_DTO.setEmail("newemail@gmail.com");
        mvc.perform( MockMvcRequestBuilders
                .put("/api/employees/{id}", EMPLOYEE_DTO.getId())
                .content(asJsonString(EMPLOYEE_DTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertDoesNotThrow(()-> employeeRepository.findById(EMPLOYEE_DTO.getId()));

        Employees employee = employeeRepository.findById(EMPLOYEE_DTO.getId()).get();
        assertEquals(EMPLOYEE_DTO.getEmail(), employee.getEmail());
    }

    @Test
    public void updateEmployeeShouldReturnNotFoundWhenIdIsInvalid() throws Exception
    {
        EMPLOYEE_DTO.setId(1000L);
        EMPLOYEE_DTO.setEmail("newemail@gmail.com");
        mvc.perform( MockMvcRequestBuilders
                .put("/api/employees/{id}", 1000)
                .content(asJsonString(EMPLOYEE_DTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    public static String asJsonString(final EmployeesDTO obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}