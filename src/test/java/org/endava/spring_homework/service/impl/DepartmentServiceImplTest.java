package org.endava.spring_homework.service.impl;

import org.endava.spring_homework.entity.Departments;
import org.endava.spring_homework.entity.Employees;
import org.endava.spring_homework.entity.Locations;
import org.endava.spring_homework.repository.DepartmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
class DepartmentServiceImplTest {

    @InjectMocks
    private DepartmentServiceImpl departmentService;

    @Mock
    private DepartmentRepository departmentRepository;

    private Departments department;

    @BeforeEach
    void setup(){
        department = new Departments();
    }

    @Test
    void getAllDepartments() {
        List<Departments> list = Arrays.asList(department, department);
        Mockito.when(departmentRepository.findAll()).thenReturn(list);

        List<Departments> returnedList = departmentService.getAllDepartments(true);

        Mockito.verify(departmentRepository).findAll();
        assertEquals(list, returnedList);
    }

    @Test
    void given_ExpandTrue_thenGetAllDepartmentsShouldReturnAllFields() {
        List<Departments> list = Arrays.asList(
                createDepartment(1L, "Marketig", new Employees(), new Locations()),
                    createDepartment(2L, "IT", new Employees(), new Locations()));

        Mockito.when(departmentRepository.findAll()).thenReturn(list);

        List<Departments> returnedList = departmentService.getAllDepartments(true);

        Mockito.verify(departmentRepository).findAll();
        assertNotNull(returnedList.get(0).getId());
        assertNotNull(returnedList.get(1).getId());
        assertNotNull(returnedList.get(0).getLocation());
        assertNotNull(returnedList.get(1).getLocation());
    }

    @Test
    void given_ExpandFalse_thenGetAllDepartmentsShouldReturnAllFields() {
        List<Departments> list = Arrays.asList(
                createDepartment(null, "Marketig", null, null),
                createDepartment(null, "IT", null, null));

        Mockito.when(departmentRepository.findAllDepartmentsName()).thenReturn(list);

        List<Departments> returnedList = departmentService.getAllDepartments(false);

        Mockito.verify(departmentRepository).findAllDepartmentsName();
        assertNull(returnedList.get(0).getId());
        assertNull(returnedList.get(1).getId());
        assertNull(returnedList.get(0).getLocation());
        assertNull(returnedList.get(1).getLocation());
    }

    @Test
    void findDepartmentById() {
        Mockito.when(departmentRepository.findById(any())).thenReturn(java.util.Optional.of(department));

        Departments returnedDepartment = departmentService.findDepartmentById(department.getId());

        Mockito.verify(departmentRepository).findById(any());
        assertEquals(returnedDepartment, department);
    }

    @Test
    void findDepartmentByInvalidIdShouldThrowNoSuchElementException() {
        Mockito.when(departmentRepository.findById(any())).thenThrow(NoSuchElementException.class);

        assertThrows(NoSuchElementException.class, ()-> departmentService.findDepartmentById(department.getId()));
        Mockito.verify(departmentRepository).findById(any());
    }

    @Test
    void addDepartment() {
        Mockito.when(departmentRepository.save(department)).thenReturn(department);

        Departments returnedDepartment = departmentService.addDepartment(department);

        Mockito.verify(departmentRepository).save(department);
        assertEquals(department, returnedDepartment);
    }

    @Test
    void deleteDepartmentById() {
        departmentService.deleteDepartmentById(any());

        Mockito.verify(departmentRepository).deleteById(any());
    }

    private Departments createDepartment(Long id, String name, Employees employees, Locations locations) {
        return Departments
                .builder()
                .id(id)
                .departmentName(name)
                .manager(employees)
                .location(locations)
                .build();
    }
}