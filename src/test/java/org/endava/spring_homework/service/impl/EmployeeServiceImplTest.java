package org.endava.spring_homework.service.impl;

import org.endava.spring_homework.DTO.EmployeesDTO;
import org.endava.spring_homework.entity.Departments;
import org.endava.spring_homework.entity.Employees;
import org.endava.spring_homework.entity.Locations;
import org.endava.spring_homework.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.core.annotation.Order;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import static org.endava.spring_homework.EmployeeSupplier.createEmployee;
import static org.endava.spring_homework.EmployeeSupplier.createEmployeeDTO;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
public class EmployeeServiceImplTest {

    @InjectMocks
    private EmployeeServiceImpl employeeService;

    @Mock
    private EmployeeRepository employeeRepository;

    public EmployeesDTO employeeDTO;
    private Employees employee;

    @BeforeEach
    public void setup(){
        employee = createEmployee();
        employeeDTO = createEmployeeDTO();
    }

    @Test
    void getAllEmployees() {
        List<Employees> list = Arrays.asList(employee, employee);
        Mockito.when(employeeRepository.findAll()).thenReturn(list);

        List<Employees> returnedList = employeeService.getAll(true);

        Mockito.verify(employeeRepository).findAll();
        assertEquals(list, returnedList);
    }

    @Test
    void given_ExpandTrue_thenGetAllEmployeesShouldReturnAllFields() {
        List<Employees> list = Collections.singletonList(employee);

        Mockito.when(employeeRepository.findAll()).thenReturn(list);

        System.out.println(employee);
        List<Employees> returnedList = employeeService.getAll(true);

        System.out.println(returnedList.get(0));
        Mockito.verify(employeeRepository).findAll();
        assertNotNull(returnedList.get(0).getId());
        assertNotNull(returnedList.get(0).getEmail());
    }

    @Test
    void given_ExpandFalse_thenGetAllEmployeesShouldReturnAllFields() {
        List<Employees> list = Collections.singletonList(createPartialEmployee("Max", "Per"));

        Mockito.when(employeeRepository.getNameAndDepartmentAll()).thenReturn(list);

        List<Employees> returnedList = employeeService.getAll(false);

        Mockito.verify(employeeRepository).getNameAndDepartmentAll();
        assertNull(returnedList.get(0).getId());
        assertNull(returnedList.get(0).getEmail());
    }

    private Employees createPartialEmployee(String fistName, String lastName) {
        return Employees.builder()
                .firstName(fistName)
                .lastName(lastName)
                .department(createDepartment())
                .build();
    }

    private Departments createDepartment() {
        return Departments.builder()
                .id(1L)
                .build();
    }

    @Test
    void findEmployeeById() {
        Mockito.when(employeeRepository.findById(any())).thenReturn(java.util.Optional.of(employee));

        Employees returnedEmployee = employeeService.findEmployeeById(employee.getId());

        Mockito.verify(employeeRepository).findById(any());
        assertEquals(returnedEmployee, employee);
    }

    @Test
    void findEmployeeByInvalidIdShouldThrowNoSuchElementException() {
        Mockito.when(employeeRepository.findById(any())).thenThrow(NoSuchElementException.class);

        assertThrows(NoSuchElementException.class, ()-> employeeService.findEmployeeById(employee.getId()));
        Mockito.verify(employeeRepository).findById(any());
    }

    @Test
    void addEmployee() {
        Mockito.when(employeeRepository.save(employee)).thenReturn(employee);

        Employees returnedEmployee = employeeService.saveEmployee(employee);

        Mockito.verify(employeeRepository).save(employee);
        assertEquals(employee, returnedEmployee);
    }

    @Test
    void deleteEmployeeById() {
        employeeService.deleteEmployee(any());

        Mockito.verify(employeeRepository).deleteById(any());
    }
}
