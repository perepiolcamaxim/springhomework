package org.endava.spring_homework;

import org.endava.spring_homework.DTO.EmployeesDTO;
import org.endava.spring_homework.entity.Employees;
import org.endava.spring_homework.entity.Jobs;
import java.sql.Date;
import java.time.LocalDate;

public class EmployeeSupplier {
    public static Employees createEmployee(){
            return Employees.builder()
                    .id(1L)
                    .email("perepiolcamaxim@gmail.com")
                    .firstName("Per")
                    .lastName("Max")
                    .phoneNumber("078751342")
                    .build();
        }

    public static EmployeesDTO createEmployeeDTO() {
        return EmployeesDTO.builder()
                .id(3L)
                .firstName("Per")
                .lastName("Max")
                .email("permax@gmail.com")
                .phoneNumber("078701328")
                .commissionPCT(null)
                .departmentId(1)
                .job(new Jobs("AD_PRES"))
                .hireDate(Date.valueOf(LocalDate.now()))
                .salary(12)
                .managerId(1L)
                .build();
    }
}
